pragma solidity ^0.4.24;

contract MoniContract {
    
//-------- General information -----------------------------------------------
    uint active_stage;   //current active stage
    uint stop_contract;
    address public token_address;
    uint public token_rate = 100;
    uint public current_stage;
    
    
    
    // Investors struct - write here when signup of investor
    struct Investors {
        uint total_invest;
        bool active;
        uint locked_amount;
        uint unlocked_amount;
        uint total_withdraw;
        uint signup;
    
    }
    
    mapping(address => Investors) public _investors;
    
    //Administrators 
    struct Admins {
        bool active;
    }
    
    mapping(address => Admins) public _admins;
    
    //completed stages
    
    struct Stages_completed {
        uint stages_completed;
    }
    
    mapping(uint => Stages_completed) public _stage_completed;
    //General information about stages
    struct stage_set0 {
        bool active;
        uint start_date;
        uint bonus; //bonus in percent
        uint duration;
        uint sold_token;
        uint vesting_time;
        uint min;
        uint max;
        uint total_tokens;
    }
    
    stage_set0 public stage0_setting = stage_set0({active:false,
                                            start_date:0,
                                            bonus:50,
                                            duration:0,
                                            sold_token:0,
                                            vesting_time:9,
                                            min:0,
                                            max:0,
                                            total_tokens:0
    });
    struct stage_set1 {
        bool active;
        uint start_date;
        uint bonus;
        uint duration;
        uint sold_token;
        uint vesting_time;
        uint min;
        uint max;
        uint total_tokens;
    }
    
    stage_set1 public stage1_setting = stage_set1({active:false,
                                            start_date:0,
                                            bonus:30,
                                            duration:0,
                                            sold_token:0,
                                            vesting_time:9,
                                            min:0,
                                            max:0,
                                            total_tokens:0
    });
    struct stage_set2 {
        bool active;
        uint start_date;
        uint bonus;
        uint duration;
        uint sold_token;
        uint vesting_time;
        uint min;
        uint max;
        uint total_tokens;
    }
    
    stage_set2 public stage2_setting = stage_set2({active:false,
                                            start_date:0,
                                            bonus:20,
                                            duration:0,
                                            sold_token:0,
                                            vesting_time:9,
                                            min:0,
                                            max:0,
                                            total_tokens:0
    });
    struct stage_set3 {
        bool active;
        uint start_date;
        uint bonus;
        uint duration;
        uint sold_token;
        uint vesting_time;
        uint min;
        uint max;
        uint total_tokens;
    }
    
    stage_set3 public stage3_setting = stage_set3({active:false,
                                            start_date:0,
                                            bonus:15,
                                            duration:0,
                                            sold_token:0,
                                            vesting_time:6,
                                            min:0,
                                            max:0,
                                            total_tokens:0
    });
    struct stage_set4 {
       bool active;
        uint start_date;
        uint bonus;
        uint duration;
        uint sold_token;
        uint vesting_time;
        uint min;
        uint max;
        uint total_tokens;
    }
    
    stage_set4 public stage4_setting = stage_set4({active:false,
                                            start_date:0,
                                            bonus:10,
                                            duration:0,
                                            sold_token:0,
                                            vesting_time:6,
                                            min:0,
                                            max:0,
                                            total_tokens:0
    });
    struct stage_set5 {
        bool active;
        uint start_date;
        uint bonus;
        uint duration;
        uint sold_token;
        uint vesting_time;
        uint min;
        uint max;
        uint total_tokens;
    }
    
    stage_set5 public stage5_setting = stage_set5({active:false,
                                            start_date:0,
                                            bonus:5,
                                            duration:0,
                                            sold_token:0,
                                            vesting_time:6,
                                            min:0,
                                            max:0,
                                            total_tokens:0
    });
    struct stage_set6 {
        bool active;
        uint start_date;
        uint bonus;
        uint duration;
        uint sold_token;
        uint vesting_time;
        uint min;
        uint max;
        uint total_tokens;
    }
    
   
   stage_set6 public stage6_setting = stage_set6({active:false,
                                            start_date:0,
                                            bonus:0,
                                            duration:0,
                                            sold_token:0,
                                            vesting_time:12,
                                            min:0,
                                            max:0,
                                            total_tokens:0
   });
    
    
    //struct for saving many in every stage_set by Investors
    
    struct stage_invest0{
        uint locked_amount;
        uint unlocked_amount;
        uint dividends;
        uint total_invest_stage;
    }
    
    struct stage_invest1 {
        uint locked_amount;
        uint unlocked_amount;
        uint dividends;
        uint total_invest_stage;
    }
    
    struct stage_invest2{
        uint locked_amount;
        uint unlocked_amount;
        uint dividends;
        uint total_invest_stage;
    }
    
    struct stage_invest3{
        uint locked_amount;
        uint unlocked_amount;
        uint dividends;
        uint total_invest_stage;
    }
    
    struct stage_invest4{
        uint locked_amount;
        uint unlocked_amount;
        uint dividends;
        uint total_invest_stage;
    }
    
    struct stage_invest5{
        uint locked_amount;
        uint unlocked_amount;
        uint dividends;
        uint total_invest_stage;
    }
    
    struct stage_invest6{
        uint locked_amount;
        uint unlocked_amount;
        uint dividends;
        uint total_invest_stage;
    }
    
    mapping(address=>stage_invest0) public _stage_invest0;
    mapping(address=>stage_invest1) public _stage_invest1;
    mapping(address=>stage_invest2) public _stage_invest2;
    mapping(address=>stage_invest3) public _stage_invest3;
    mapping(address=>stage_invest4) public _stage_invest4;
    mapping(address=>stage_invest5) public _stage_invest5;
    mapping(address=>stage_invest6) public _stage_invest6;
    
    
//========================== Function for getting of ether =====================

    function () payable {
        // assert(_investors[msg.sender].signup == 1);
        if (current_stage == 1){
            uint bonus0 = msg.value*token_rate;
            bonus0 = bonus0 + bonus0/100*stage0_setting.bonus;
            uint investing_amount0 = msg.value+bonus0;
            _stage_invest0[msg.sender].locked_amount = _stage_invest0[msg.sender].locked_amount + investing_amount0;
            _stage_invest0[msg.sender].total_invest_stage = _stage_invest0[msg.sender].total_invest_stage + msg.value;
            _investors[msg.sender].total_invest = _investors[msg.sender].total_invest + msg.value;
            _investors[msg.sender].locked_amount = _investors[msg.sender].locked_amount + investing_amount0;
            stage0_setting.total_tokens = stage0_setting.total_tokens + investing_amount0;
        }
        if (current_stage == 2){
            uint bonus1 = msg.value*token_rate;
            bonus1 = bonus1 + bonus1/100*stage1_setting.bonus;
            uint investing_amount1 = msg.value+bonus1;
            _stage_invest1[msg.sender].locked_amount = _stage_invest1[msg.sender].locked_amount + investing_amount1;
            _stage_invest1[msg.sender].total_invest_stage = _stage_invest1[msg.sender].total_invest_stage + msg.value;
            _investors[msg.sender].total_invest = _investors[msg.sender].total_invest + msg.value;
            _investors[msg.sender].locked_amount = _investors[msg.sender].locked_amount + investing_amount1;
            stage1_setting.total_tokens = stage1_setting.total_tokens + investing_amount1;
            
        }
         if (current_stage == 3){
            uint bonus2 = msg.value*token_rate;
            bonus2 = bonus2 + bonus2/100*stage2_setting.bonus;
            uint investing_amount2 = msg.value+bonus2;
            _stage_invest2[msg.sender].locked_amount = _stage_invest2[msg.sender].locked_amount + investing_amount2;
            _stage_invest2[msg.sender].total_invest_stage = _stage_invest2[msg.sender].total_invest_stage + msg.value;
            _investors[msg.sender].total_invest = _investors[msg.sender].total_invest + msg.value;
            _investors[msg.sender].locked_amount = _investors[msg.sender].locked_amount + investing_amount1;
            stage2_setting.total_tokens = stage2_setting.total_tokens + investing_amount2;
        }
         if (current_stage == 4){
            uint bonus3 = msg.value*token_rate;
            bonus3 = bonus3 + bonus3/100*stage3_setting.bonus;
            uint investing_amount3 = msg.value+bonus3;
            _stage_invest3[msg.sender].locked_amount = _stage_invest3[msg.sender].locked_amount + investing_amount3;
            _stage_invest3[msg.sender].total_invest_stage = _stage_invest3[msg.sender].total_invest_stage + msg.value;
            _investors[msg.sender].total_invest = _investors[msg.sender].total_invest + msg.value;
            _investors[msg.sender].locked_amount = _investors[msg.sender].locked_amount + investing_amount1;
            stage3_setting.total_tokens = stage3_setting.total_tokens + investing_amount3;
        }
         if (current_stage == 5){
            uint bonus4 = msg.value*token_rate;
            bonus4 = bonus4 + bonus5/100*stage4_setting.bonus;
            uint investing_amount4 = msg.value+bonus4;
            _stage_invest4[msg.sender].locked_amount = _stage_invest4[msg.sender].locked_amount + investing_amount4;
            _stage_invest4[msg.sender].total_invest_stage = _stage_invest4[msg.sender].total_invest_stage + msg.value;
            _investors[msg.sender].total_invest = _investors[msg.sender].total_invest + msg.value;
            _investors[msg.sender].locked_amount = _investors[msg.sender].locked_amount + investing_amount4;
            stage4_setting.total_tokens = stage0_setting.total_tokens + investing_amount4;
        }
        if (current_stage == 6){
            uint bonus5 = msg.value*token_rate;
            bonus5 = bonus5 + bonus5/100*stage5_setting.bonus;
            uint investing_amount5 = msg.value+bonus5;
            _stage_invest5[msg.sender].locked_amount = _stage_invest5[msg.sender].locked_amount + investing_amount5;
            _stage_invest5[msg.sender].total_invest_stage = _stage_invest5[msg.sender].total_invest_stage + msg.value;
            _investors[msg.sender].total_invest = _investors[msg.sender].total_invest + msg.value;
            _investors[msg.sender].locked_amount = _investors[msg.sender].locked_amount + investing_amount5;
            stage5_setting.total_tokens = stage0_setting.total_tokens + investing_amount5;
        }
        if (current_stage == 7){
            // need to change only for manual payments
             _stage_invest6[msg.sender].total_invest_stage = _stage_invest6[msg.sender].total_invest_stage + msg.value;
          
           
        }
    }

   
   modifier OnlyAdmin(address _admin_address) {
       assert(_admins[_admin_address].active = true);
       _;
   }
   
       
   function set_rate(uint _rate) public OnlyAdmin(msg.sender)  {
       uint token_rate = _rate;
   }
   
   
    //add investor to struct of Investors
    function set_investor(address _address) public returns (string) {
    if (_investors[_address].signup != 1) {
        _investors[_address].active = true;
        _investors[_address].signup = 1;
    }
    else {return "Allready Singup";}
    }
    
    //set unset investors active
    function set_investor_active(address _address, bool _active) public OnlyAdmin(msg.sender) {
        assert(_investors[_address].signup == 1);
        _investors[_address].active = _active;
    }
    //set admins for testing
    function set_admins(address _address) public {
        _admins[_address].active = true;
    }
    
    //set stop contract
    
    function set_stop(uint _status) public OnlyAdmin(msg.sender)  {
        stop_contract = 1;
    }
    
    //set stages completed
    
    function set_stage_completed(uint _completed_stage)  OnlyAdmin(msg.sender) {
        _stage_completed[_completed_stage].stages_completed = _completed_stage;
    }
    
    //set current_stage
    
    function set_current_stage(uint _current_stage, uint _min, uint _max, uint _duration, uint _sold_token) public OnlyAdmin(msg.sender) {
        require(current_stage == _current_stage-1);
        current_stage = _current_stage;
        uint start_date = block.timestamp;
        if (_current_stage == 1){
            stage0_setting.active = true;
            stage0_setting.start_date = start_date;
            stage0_setting.duration = _duration;
            stage0_setting.min = _min;
            stage0_setting.max = _max;
            stage0_setting.sold_token = _sold_token;
        }
        
        if (_current_stage == 2){
            stage1_setting.active = true;
            stage1_setting.start_date = now;
            stage1_setting.duration = _duration;
            stage1_setting.min = _min;
            stage1_setting.max = _max;
            stage1_setting.sold_token = _sold_token;
           
        }
        if (_current_stage == 3){
            stage2_setting.active = true;
            stage2_setting.start_date = now;
            stage2_setting.duration = _duration;
            stage2_setting.min = _min;
            stage2_setting.max = _max;
            stage2_setting.sold_token = _sold_token;
           
        }
        if (_current_stage == 4){
            stage3_setting.active = true;
            stage3_setting.start_date = now;
            stage3_setting.duration = _duration;
            stage3_setting.min = _min;
            stage3_setting.max = _max;
            stage3_setting.sold_token = _sold_token;
           
        }
        if (_current_stage == 5){
            stage4_setting.active = true;
            stage4_setting.start_date = now;
            stage4_setting.duration = _duration;
            stage4_setting.min = _min;
            stage4_setting.max = _max;
            stage4_setting.sold_token = _sold_token;
           
        }
        if (_current_stage == 6){
            stage5_setting.active = true;
            stage5_setting.start_date = now;
            stage5_setting.duration = _duration;
            stage5_setting.min = _min;
            stage5_setting.max = _max;
            stage5_setting.sold_token = _sold_token;
           
        }
        if (_current_stage == 7){
            stage6_setting.active = true;
            stage6_setting.start_date = now;
            stage6_setting.duration = _duration;
            stage6_setting.min = _min;
            stage6_setting.max = _max;
            stage6_setting.sold_token = _sold_token;
            
        }
        
    }
    
    //set tokens for investor
    
    function add_tokens_to_investor(address _address, uint _stage, uint _tokens) public OnlyAdmin(msg.sender) {
        assert(_investors[_address].signup == 1);
        if (current_stage == 1){
            _stage_invest0[_address].locked_amount = _stage_invest0[_address].locked_amount + _tokens;
            stage0_setting.total_tokens = stage0_setting.total_tokens + _tokens;
        }
        if (current_stage == 2){
            _stage_invest1[_address].locked_amount = _stage_invest1[_address].locked_amount + _tokens;
            stage1_setting.total_tokens = stage1_setting.total_tokens + _tokens;
        }
        if (current_stage == 3){
            _stage_invest2[_address].locked_amount = _stage_invest2[_address].locked_amount + _tokens;
            stage2_setting.total_tokens = stage2_setting.total_tokens + _tokens;
        }
        if (current_stage == 4){
            _stage_invest3[_address].locked_amount = _stage_invest3[_address].locked_amount + _tokens;
            stage3_setting.total_tokens = stage3_setting.total_tokens + _tokens;
        }
        if (current_stage == 5){
            _stage_invest4[_address].locked_amount = _stage_invest4[_address].locked_amount + _tokens;
            stage4_setting.total_tokens = stage4_setting.total_tokens + _tokens;
        }
        if (current_stage == 6){
            _stage_invest5[_address].locked_amount = _stage_invest5[_address].locked_amount + _tokens;
            stage5_setting.total_tokens = stage5_setting.total_tokens + _tokens;
        }
        if (current_stage == 7){
            _stage_invest6[_address].locked_amount = _stage_invest6[_address].locked_amount + _tokens;
            stage6_setting.total_tokens = stage6_setting.total_tokens + _tokens;
        }
    }
    
    //test of gas insert investor from array
    
    function set_investors_array(address[] _investors_list) public {
        uint arrayLength = _investors_list.length;
        for (uint i=0; i<arrayLength; i++) {
            _investors[_investors_list[i]].active = true;
            _investors[_investors_list[i]].signup = 1;
            
        }
    }
    
    
}
